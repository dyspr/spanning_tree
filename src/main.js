var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var amountOfNodes = 29
var nodes

var startNode
var tree = []
var edges = []
var newEdge
var nodeToRemove
var maxSize = 0.025
var resolution = 64

var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  nodes = createNodes(amountOfNodes)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < nodes.length; i++) {
    fill(128)
    noStroke()
    push()
    translate(windowWidth * 0.5 - boardSize * 0.5 + nodes[i][0] * boardSize, windowHeight * 0.5 - boardSize * 0.5 + nodes[i][1] * boardSize)
    ellipse(0, 0, nodes[i][2] * boardSize * maxSize)
    pop()
  }

  for (var i = 0; i < edges.length; i++) {
    drawEdge(edges[i][0][0], edges[i][0][1], edges[i][0][2], edges[i][1][0], edges[i][1][1], edges[i][1][2], resolution)
  }

  frame += deltaTime * 0.025
  if (frame > 1) {
    frame = 0

    if (startNode === undefined) {
      startNode = pickStartingNode(nodes)
      tree.push([startNode[0], startNode[1], startNode[2]])
      nodes.splice(startNode[3], 1)
    } else {
      if (nodes.length !== 0) {
        edge = getMinimumDistance(calculateDistances(tree, nodes)[0], calculateDistances(tree, nodes)[1])
        tree.push([edge[1][0], edge[1][1], edge[1][2]])
        edges.push(edge)
        nodeToRemove = getIndexOfNode(edge, nodes)
        nodes.splice(nodeToRemove, 1)
      } else {
        setTimeout(function() {
          amountOfNodes = 13 + Math.floor(Math.random() * (128 - 13))
          maxSize = 1 / amountOfNodes
          resolution = 4096  / amountOfNodes
          nodes = createNodes(amountOfNodes)
          tree = []
          edges = []
          startNode = pickStartingNode(nodes)
          tree.push([startNode[0], startNode[1], startNode[2]])
          nodes.splice(startNode[3], 1)
        }, 2000)
      }
    }
  }
}

function mousePressed() {
  if (startNode === undefined) {
    startNode = pickStartingNode(nodes)
    tree.push([startNode[0], startNode[1], startNode[2]])
    nodes.splice(startNode[3], 1)
  } else {
    if (nodes.length !== 0) {
      edge = getMinimumDistance(calculateDistances(tree, nodes)[0], calculateDistances(tree, nodes)[1])
      tree.push([edge[1][0], edge[1][1], edge[1][2]])
      edges.push(edge)
      nodeToRemove = getIndexOfNode(edge, nodes)
      nodes.splice(nodeToRemove, 1)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function drawEdge(x1, y1, s1, x2, y2, s2, num) {
  for (var i = 0; i < num; i++) {
    var biggerNode = Math.max(s1, s2)
    var smallerNode = Math.min(s1, s2)
    fill(255)
    noStroke()
    push()
    translate(windowWidth * 0.5 - boardSize * 0.5 + boardSize * x1 + ((windowWidth * 0.5 - boardSize * 0.5 + boardSize * x2) - (windowWidth * 0.5 - boardSize * 0.5 + boardSize * x1)) * (1 / num) * i, windowHeight * 0.5 - boardSize * 0.5 + boardSize * y1 - ((windowHeight * 0.5 - boardSize * 0.5 + boardSize * y1) - (windowHeight * 0.5 - boardSize * 0.5 + boardSize * y2)) * (1 / num) * i)
    if (s1 > s2) {
      ellipse(0, 0, s1 * boardSize * maxSize - (biggerNode - smallerNode) * (1 / num) * i * boardSize * maxSize)

    } else {
      ellipse(0, 0, s1 * boardSize * maxSize + (biggerNode - smallerNode) * (1 / num) * i * boardSize * maxSize)

    }
    pop()
  }
}

function getIndexOfNode(edge, nodes) {
  var indexOfNode
  for (var i = 0; i < nodes.length; i++) {
    if (arraysEqual(nodes[i], edge[1]) === true) {
      indexOfNode = i
    }
  }
  return indexOfNode
}

function arraysEqual(a, b) {
  if (a === b) return true
  if (a == null || b == null) return false
  if (a.length !== b.length) return false
  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false
  }
  return true
}

function getMinimumDistance(distances, nodePairs) {
  var minimum = Math.min(...distances)
  var nodePair = nodePairs[distances.indexOf(minimum)]
  return nodePair
}

function calculateDistances(tree, nodes) {
  var distances = []
  var nodePairs = []
  for (var i = 0; i < tree.length; i++) {
    for (var j = 0; j < nodes.length; j++) {
      distances.push(dist(tree[i][0], tree[i][1], nodes[j][0], nodes[j][1]))
      nodePairs.push([[tree[i][0], tree[i][1], tree[i][2]], [nodes[j][0], nodes[j][1], nodes[j][2]]])
    }
  }
  return [distances, nodePairs]
}

function pickStartingNode(nodes) {
  var rand = Math.floor(Math.random() * nodes.length)
  var start = [nodes[rand][0], nodes[rand][1], nodes[rand][2], rand]
  return start
}

function createNodes(amount) {
  var nodes = []
  for (var i = 0; i < amount; i++) {
    var posX = 0.1 + 0.8 * Math.random()
    var posY = 0.1 + 0.8 * Math.random()
    var size = 0.2 + 0.8 * Math.random()
    nodes.push([posX, posY, size])
  }
  return nodes
}
